$Id

This module is used to create email campaigns by providing a node type
that includes an email message and an email sending form, along with a form
with which to gather information on the mail sender.  The email will be sent and
signed by the sender with a link to the web page from which it was sent.

To use this module, simply create content of the Email Campaign type
and write the email subject in the subject field, the message in the message field, 
the (optional) destination email in the email field and some instructions if needed.
		
If no email is provided in the destination email field, the user will be provided
with a blank field in which to write an email address where the message should be sent.

Users with appropriate permissions will be able to create and edit email campaigns, and
users with the administer email campaign permission will be able to see how many times a 
campaign was sent as well as delete bad signatures.

