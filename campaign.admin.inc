<?php

/**
 * @file
 * Administration page callbacks and form builders for the campaign module
 */

/**
 * Page callback.
 * @param $nid
 *  The node id of the email campaign node for which we want to see the signatures
 * @return
 *  A form built according to the user's actions
 */
function campaign_admin_signatures($nid = NULL, &$form_state = NULL) {
	if ($nid == NULL) {
	 return t('This is not a valid campaign.');
	}
  return drupal_get_form('campaign_admin_sentforms', $nid);
}

/**
 * Signatures form builder.
 * 
 * If storage delete value is set, build the delete confirm form.  Otherwise build the checkbox table.
 * @param $nid
 *  The node id of the email campaign node for which we want to see the signatures
 */
function campaign_admin_sentforms(&$form_state, $nid = 0) {
	// True if form is rebuilt.  Build delete confirm form.
	if ($form_state['storage']['delete'])
	{
		$form_state['storage']['delete'] = FALSE;
		$nid = array_pop(explode('/', $_GET['q']));
		drupal_set_title(t('Delete these signatures?'));
    // $mids are ids for each mail sent.
		$mids = $form_state['values']['signatures'];
    // Prepare a list of names from the signatures to show user these will be deleted.
		$names = array();
		foreach ($mids as $mid) {
		  $result = db_query('SELECT first_name, last_name FROM {campaign_sent} WHERE mid = %d', $mid);
		  while ($a_name = db_fetch_array($result)) {
		    $names[] = check_plain(implode(' ', $a_name));
		  }
		}
		$form['item'] = array(
		  '#value' => theme('item_list', $names),
		  '#suffix' => t('This action cannot be undone.') . '<br/>', 
		);
		$form['#mids'] = $mids;
		$form['#nid'] = $nid;
		$form['submit'] = array(
      '#title' => t('First name'),
      '#type' => 'submit',
		  '#value' => t('Delete'),
		  '#suffix' => l(t('Cancel'), 'admin/campaign/signatures/' . $nid),
		  '#submit' => array('campaign_delete_signatures_submit'),
		  // No need to validate this form.
		  '#validate' => array(),  
    );    
		return $form;
	}
	// Form presented for the first time. Build the signature list form.
	$query = 'SELECT mid, nid, uid, first_name, last_name, email, street1,
    street2, city, state_prov, postal_code FROM {campaign_sent} WHERE nid = %d';
	$result = pager_query($query, 50, 0, NULL, $nid);
	$signatures = array();
	while ($signature = db_fetch_object($result)) {
	  $signatures[$signature->mid] = '';
	  $form['first_name'][$signature->mid] = array('#value' => check_plain($signature->first_name));
	  $form['last_name'][$signature->mid] = array('#value' => check_plain($signature->last_name));
	  $form['email'][$signature->mid] = array('#value' => check_plain($signature->email));
	  $form['street1'][$signature->mid] = array('#value' => check_plain($signature->street1));
	  $form['street2'][$signature->mid] = array('#value' => check_plain($signature->street2));
	  $form['city'][$signature->mid] = array('#value' => check_plain($signature->city));
	  $form['state_prov'][$signature->mid] = array('#value' => check_plain($signature->state_prov));
	  $form['postal_code'][$signature->mid] = array('#value' => check_plain($signature->postal_code));
	}
	$form['signatures'] = array('#type' => 'checkboxes', '#options' => $signatures);
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Remove items'),
    '#suffix' => l(t('Back to campaign'), 'node/' . $nid),
  );
  $form['#theme'] = 'campaign_signaturesform';
  return $form;	
}

/**
 * Validate campaign_admin_sentforms.
 * Check if any signatures have been selected to delete.
 */
function campaign_admin_sentforms_validate($form, &$form_state) {
  $signatures = array_filter($form_state['values']['signatures']);
  if (count($signatures) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

/**
 * Theme the signatures form to a table.
 */
function theme_campaign_signaturesform($form){
  $has_posts = isset($form['first_name']) && is_array($form['first_name']);
  $select_header = $has_posts ? theme('table_select_header_cell') : '';
  $header = array(
    $select_header, t('First Name'), t('Last Name'), t('Email'), t('Street 1'),
    t('Street 2'), t('City'), t('State/Province'), t('Postal/Zip code')
  );
  if ($has_posts) {
  	foreach (element_children($form['first_name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['signatures'][$key]);
      $row[] = drupal_render($form['first_name'][$key]);
      $row[] = drupal_render($form['last_name'][$key]);
      $row[] = drupal_render($form['email'][$key]);
      $row[] = drupal_render($form['street1'][$key]);
      $row[] = drupal_render($form['street2'][$key]);
      $row[] = drupal_render($form['city'][$key]);
      $row[] = drupal_render($form['state_prov'][$key]);
      $row[] = drupal_render($form['postal_code'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No signatures available.'), 'colspan' => '9'));
  }
  $output = theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  $output .= drupal_render($form);
  return $output;
}

/**
 * Submit fonction for signature form.
 * 
 * The form is rebuilt with storage value indicating the need
 * to build the delete confirm form.
 */
function campaign_admin_sentforms_submit($form, &$form_state) {
  // Set storage to rebuild the form.
  $form_state['storage']['delete'] = TRUE;
}

/**
 * Submit handler for confirm delete form.
 */
function campaign_delete_signatures_submit($form, &$form_state) {
  $count = 0;
  foreach ($form['#mids'] as $mid) {
    db_query('DELETE FROM {campaign_sent} WHERE mid = %d', $mid);
    $count++;
  }
  // Reduce the number of time this campaign was sent by the number of signatures deleted.
  db_query('UPDATE campaign_content SET sent = sent - %d WHERE nid = %d', $count, $form['#nid']);  
  drupal_set_message(t('The selected signatures have been deleted.'));
}